#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging.handlers
import os
import random
import configparser
import sys
import asyncio
import time

from datetime import datetime

import telegram
import pytz
import emoji


BITUCAS_LOGGING_LEVEL = int(os.getenv("BITUCAS_LOGGING_LEVEL", 10))
logger = logging.getLogger(__name__)
logger.setLevel(BITUCAS_LOGGING_LEVEL)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(BITUCAS_LOGGING_LEVEL)
S_HANDLER.setFormatter(formatter)
logger.addHandler(S_HANDLER)


sao_paulo_tz = pytz.timezone("America/Sao_Paulo")


COACH_QUOTES = [
    "Nunca caminhe sem um documento nas mãos. Pessoas com documentos em uma das mãos parecem funcionários ocupadíssimos que se dirigem para reuniões importantes. #democouch",
    "Sempre leve algum material para casa, isso causa a falsa impressão de que você trabalha mais horas do que você costuma trabalhar. #democouch",
    "Tenha uma mesa bagunçada. Quando sua mesa está bagunçada parece que você está trabalhando duramente. #democouch",
    "Construa pilhas enormes de documentos em torno de seu espaço de trabalho para parecer ocupado. #democouch",
    "Ao observador, o trabalho do ano passado parece o mesmo que o trabalho de hoje; é o volume que conta. Se você souber que alguém está vindo à sua mesa finja que está procurando algum papel. #democouch",
    "Nunca responda ao seu telefone se você tiver o correio de voz. As pessoas não te ligam para te dar nada além de mais trabalho. #democouch",
    "Selecione todas suas chamadas sempre através do correio de voz. #democouch",
    "Se alguém deixar uma mensagem do correio de voz para você e se for para trabalho, responda durante a hora do almoço quando você sabe que eles não estão lá. #democouch",
    "Você deve estar sempre parecendo impaciente e irritado, para dar ao seu chefe a impressão de que você está realmente ocupado. #democouch",
    "Sempre deixe o escritório mais tarde, especialmente se o seu chefe estiver por perto. #democouch",
    "Sempre passe na frente da sala do seu Chefe quando estiver indo embora. #democouch",
    "Programe os e-mails importantes pare serem enviados bem tarde (por exemplo 21:35, 6:00, etc…) e durante feriados e finais de semana. #democouch",
    "Fale sozinho quando tiver muita gente por perto, dando a impressão de que você está sob pressão extrema. #democouch",
    "Empilhar documentos em cima da mesa não é o bastante. Ponha vários livros no chão. (os manuais grossos do computador são melhores ainda). #democouch",
    "Procure no dicionário palavras difíceis. Construa frases e use-as quando estiver conversando com o seu chefe. Lembre-se: ele não tem que entender o que você diz, desde que o que você diga dê a entender de que você está certo. #democouch",
    "Querido Papai Noel. Nesse natal eu queria um mindset de filho da puta para poder derrubar o meu chefe. #democouch",
    "Mensagem urgente de WhatsApp: Responda 3h depois que só prioriza ferramentas do trabalho. #democouch",
    "Mensagem urgente do trabalho: Responda 2h depois que estava numa ligação no WhatsApp. #democouch",
    "Mais dicas no livro Demo Couch: Infernizando seu chefe no home office. #democouch",
    "Nesse verão não deixe de beber muita água no trabalho. Não por causa da saúde, mas sim para poder ir ao banheiro sempre que um problema aparecer. #democouch",
    "Bora Timeee!!! Não deixe ninguém falar que vc é fracassado. Essa é sua jornada. Seja proativo, e fale antes. #democouch",
    "Bora timeeeeee!!! Essas casas não serão desapropriadas sozinhas. Bora terraplanar esse mindset derrotista e construir duas torres de auto estima. #democouch",
    "Dicas Home Office: marque reunião de 2 horas, resolva em 30 minutos, fique logado na sala pra trancar sua agenda. Responda que está em reunião e se cuide. #democouch",
    "Bora timeee!!! Se o Instagram caiu, tire foto do seu temake, imprima e mande por correio. Seja menos fazendo mais. #democouch",
    "Isso na privada não são suas fezes, é o reflexo da sua cara expelindo esse mindset modorrento. Escove os dentes e grite no espelho: 'EU SOU O ROCKY BALBOA'. #democouch",
    "Bora timeeee!!! A Faria Lima não anda sozinha. Seja o Redbull da sua vida!!! #democouch",
    "Bora Time!!!!! O jogo tá ganho, mas vamos pra cima fazer mais um gol!!! Goleiro que não franga é pq não foi pra bola!!! #democouch",
    "Vamo timeeee!!! Bora resignificar ASAP essa serotonina em Taffman-E e disrruptivar todo esse mindset modorrento. #democouch",
    "Bora Timeeee!!!! Seja seu próprio Facebook e viralize esse mindset mequetrefe. #democouch",
    "Timeeee, quero ver todo mundo com o tridente na mão que hj está o inferno puro. Seja o protagonista nessa porra de filme preto e branco que é sua vida. #democouch",
    "Lute como nunca, perca como sempre. #democouch",
    "Não deixe uma frase motivacional melhorar o seu dia de merda. #democouch",
    "Você não pode mudar o seu passado, mas pode estragar o seu futuro! #democouch",
    "Se foi ruim ontem, fique tranquilo que hoje será pior. #democouch",
    "Vamos levantar? A vida não consegue te derrubar com você deitado. #democouch",
    "Não sabendo que era impossível, foi lá e soube. #democouch",
    "Só dará errado se você tentar. #democouch",
    "Uma grande jornada termina com uma bela derrota. #democouch",
    "O não você já tem, agora falta buscar a humilhação. #democouch",
    "Se alguém te ofendeu sem você merecer, volte lá e mereça! #democouch",
    "Seja o protagonista do seu fracasso. #democouch",
    "Escolha lugares para almoçar que só dê pra ir de carro, que tenha filas longas e que em média o prato chegue 30 minutos depois. Jamais, leve comida de casa, jamais. Fique o mais longe possível do ambiente de trabalho. #democouch",
    "Dia de fortalecer a economia local! Ajude o borracheiro da sua cidade, fure o pneu do seu chefe! #democouch",
    "Bom dia! Vamos que é {}, você ainda tem o resto da semana pra fracassar!",
    "{}! Feliz aniversário! Talvez você não tenha nascido hoje, mas um novo fracasso está para nascer na sua vida!",
    "{}, um ótimo dia para soltar os nudes do seu chefe em urgia de traveco!",
    "Hoje é {} meus bacanudos!",
    "São 10h de uma {} não é?! Semana praticamente encerrada ... mas só pro seu chefe!",
    "{}, um ótimo dia para ouvir em episódio do Procurando Bitucas enquanto você é encoxado no mêtro",
    "Salve! Hoje é {} e lembre-se de beber água e se masturbar na empresa. Afinal, você está sendo pago se foder mesmo!",
    "{}, Um pouco de sabedoria romana: Não importa o que você faça em vida, você ecoará como um fracasso!",
    "{} 10 da manhã e o Guerreirinho já postou 30 fotos sem camisa no twitter",
    "{}, hoje fazem 0 dias e 10 horas que o Dono está sóbrio. Alguém ligue para os alcoólicos anônimos!",
    "{}, dia de você provar que a evolução pode ir em sentido inverso!",
    "{}, lembre-se, se você não sair da cama, não há perigo de fracassar o seu dia!",
    "{}, não esqueça de entrar no Apple podcast e avaliar mal os concorrentes do Procurando Bitucas!",
    "Essa {} tem tudo pra ser um fracasso, só depende de você!",
    "Que o Sol ilumine o seu caminho de fracassos nessa {}!",
    "Faça boas escolhas e tenha esperança de fracassar nessa {}!",
    "Feliz {}! Bora cheirar sovaco no ônibus e morcegar o dia todo!",
    "Vamos! {}! Dia de fortalecer a economia local! Peça demissão para cortar despesas na firma!",
    "{}, dia de mudar o seu mindset! Hora de performar o seu Bitucas interno!",
    "{}, dia de fazer uma boa ação, fale pra mulher do seu que ela sendo corneana!",
    "{}, Beba Diabo Verde com gim para desentupir todos os seus males!",
    "{}, seja orgulhoso de sí mesmo, Deus criou o mundo em 7 dias, e você precisou apenas de 1 para estragar tudo.",
]


def day_of_week():
    now = datetime.now(sao_paulo_tz)
    if now.weekday() == 0:
        return "Segunda-feira"
    elif now.weekday() == 1:
        return "Terça-feira"
    elif now.weekday() == 2:
        return "Quarta-feira"
    elif now.weekday() == 3:
        return "Quinta-feira"
    elif now.weekday() == 4:
        return "Sexta-feira"
    elif now.weekday() == 5:
        return "Sábado"
    elif now.weekday() == 6:
        return "Domingo"


def is_under_maintenance():
    return os.getenv("BITUCAS_UNDER_MAINTENANCE", False)


def is_dry_run():
    return os.getenv("BITUCAS_DRY_RUN", False)


def config_file_path():
    return os.getenv("PB_CONFIG", "/etc/bitucas.conf")


def config_file():
    config_path = config_file_path()
    if not os.path.exists(config_path):
        raise ValueError("Could not obtain configuration file path.")
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


def remove_emojis_from_text(text):
    return emoji.replace_emoji(text, replace='')


def get_chat_id():
    token = os.getenv("BITUCAS_CHAT_ID", None)
    if not token:
        config = config_file()
        token = config["telegram"]["chat_id"]
    return token


def get_telegram_token():
    token = os.getenv("TELEGRAM_TOKEN", None)
    if not token:
        config = config_file()
        token = config["telegram"]["token"]
    return token


def show_configuration():
    telegram_token = str(get_telegram_token())[:8]
    logger.info(f"CONFIG FILE: {config_file_path()}")
    logger.info(f"TELEGRAM TOKEN: {telegram_token}")
    logger.info(f"CHAT ID: {get_chat_id()}")
    logger.info(f"UNDER MAINTENANCE: {is_under_maintenance()}")
    logger.info(f"DRY RUN: {is_dry_run()}")


async def run():
    max_retry = 5
    retry_count = 0
    show_configuration()

    if is_under_maintenance():
        logging.warning("Procurando Bitucas is under maintenance. Exiting now.")
        sys.exit(503)

    message = random.choice(COACH_QUOTES).format(day_of_week())
    logger.debug(f"coach message: {message}")

    while retry_count < max_retry:
        try:
            bot = telegram.Bot(token=get_telegram_token())
            if not is_dry_run():
                try:
                    async with bot:
                        await bot.send_message(chat_id=get_chat_id(), text=message)
                except Exception as error:
                    logger.error(f"Could not post message: {error}")
                    retry_count += 1
                    time.sleep(3)
                    continue
        except Exception as error:
            logger.error(f"Could not initialize Telegram: {error}")
            retry_count += 1
            time.sleep(3)
            continue
        break


def main():
    asyncio.run(run())


if __name__ == '__main__':
    main()
